#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

struct X_NAMING(t)
{
    X_TYPE *data;
    size_t count;
    size_t size;
};

static struct X_NAMING(t) *
X_NAMING(free)(struct X_NAMING(t) *v)
{
    if (v != NULL)
    {
        #ifdef X_CLEANUP
        for (size_t i = 0; i < v->count; i++)
            X_NAMING(cleanup)(&(v->data[i]));
        #endif

        free(v->data);
    }
    free(v);

    return NULL;
}

static struct X_NAMING(t) *
X_NAMING(alloc)(size_t size)
{
    struct X_NAMING(t) *ret = calloc(1, sizeof(struct X_NAMING(t)));

    if (ret != NULL)
    {
        ret->size = size;
        ret->data = calloc(ret->size, sizeof(X_TYPE));
        if (ret->data == NULL)
            ret = X_NAMING(free)(ret);
    }

    return ret;
}

static struct X_NAMING(t) *
X_NAMING(copy)(struct X_NAMING(t) *v)
{
    struct X_NAMING(t) *ret = NULL;

    if (v != NULL)
        ret = X_NAMING(alloc)(v->size);

    if (ret != NULL)
    {
        ret->count = v->count;
        memcpy(ret->data, v->data, v->count * sizeof(X_TYPE));
    }

    return ret;
}

static bool
X_NAMING(insert)(struct X_NAMING(t) *v, X_TYPE *item, size_t idx)
{
    bool ret = true;

    if (idx <= v->count)
    {
        if (v->size < (v->count + 1))
        {
            X_TYPE *new = realloc(v->data, v->size * 2 * sizeof(X_TYPE));
            if (new != NULL)
            {
                v->data = new;
                memset(&(v->data[v->size]), 0, v->size * sizeof(X_TYPE));
                v->size *= 2;
            }
            else
            {
                ret = false;
            }
        }

        if (ret)
        {
            if (idx == v->count)
            {
                v->data[v->count++] = *item;
            }
            else
            {
                memmove(&(v->data[idx + 1]), &(v->data[idx]),
                        (v->count - idx) * sizeof(X_TYPE));
                v->data[idx] = *item;
                v->count++;
            }
        }
    }
    else
    {
        size_t need = v->size;
        while (need < (idx + 1))
            need *= 2;

        if (need > v->size)
        {
            X_TYPE *new = realloc(v->data, need * sizeof(X_TYPE));
            if (new != NULL)
            {
                v->data = new;
                memset(&(v->data[v->size]), 0,
                       (need - v->size) * sizeof(X_TYPE));
                v->size = need;
            }
            else
            {
                ret = false;
            }
        }

        if (ret)
        {
            memcpy(&(v->data[idx]), item, sizeof(X_TYPE));
            v->count = idx + 1;
        }
    }

    return ret;
}

static bool
X_NAMING(prepend)(struct X_NAMING(t) *v, X_TYPE *item)
{
    return X_NAMING(insert)(v, item, 0);
}

static bool
X_NAMING(append)(struct X_NAMING(t) *v, X_TYPE *item)
{
    return X_NAMING(insert)(v, item, v->count);
}

static bool
X_NAMING(insert_bulk)(struct X_NAMING(t) *v, X_TYPE *item,
                      size_t quantity, size_t idx)
{
    bool ret = true;

    for (size_t i = 0; ret == true && i < quantity; i++)
        ret = X_NAMING(insert)(v, &(item[quantity - i - 1]), idx);

    return ret;
}

static bool
X_NAMING(prepend_bulk)(struct X_NAMING(t) *v, X_TYPE *item, size_t quantity)
{
    return X_NAMING(insert_bulk)(v, item, quantity, 0);
}

static bool
X_NAMING(append_bulk)(struct X_NAMING(t) *v, X_TYPE *item, size_t quantity)
{
    return X_NAMING(insert_bulk)(v, item, quantity, v->count);
}

static bool
X_NAMING(delete)(struct X_NAMING(t) *v, X_TYPE *item)
{
    bool ret = false;

    ptrdiff_t idx = (item - v->data);
    if (idx >= 0 && (unsigned)idx < v->count)
    {
        #ifdef X_CLEANUP
        X_NAMING(cleanup)(item);
        #endif

        if ((unsigned)(idx + 1) != v->count)
        {
            memmove(&(v->data[idx]), &(v->data[idx + 1]),
                    (v->count - idx - 1) * sizeof(X_TYPE));
        }
        else
        {
            memset(&(v->data[idx]), 0, sizeof(X_TYPE));
        }

        v->count--;
    }

    return ret;
}

static X_TYPE *
X_NAMING(find)(struct X_NAMING(t) *v, X_TYPE *query)
{
    X_TYPE *ret = NULL;

    for (size_t i = 0; i < v->count; i++)
    {
        if (memcmp(&(v->data[i]), query, sizeof(X_TYPE)) == 0)
        {
            ret = &(v->data[i]);
            break;
        }
    }

    return ret;
}
